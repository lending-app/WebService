const multer = require('multer');
const errors = require('boom');
const moment = require('moment');
const capitalize = require('lodash.capitalize');

const constants = require('../constants');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads');
  },
  filename: (req, file, cb) => {
    cb(null, `${req.user._id}-${req.idCheck._id}-${file.fieldname}-${moment().toISOString()}.jpg`);
  },
});

const imageFileFilter = (req, file, cb) => {
  if (!file.originalname.match(/\.(jpg)$/)) {
    return cb(errors.badData('You can upload only image files in JPG format'), false);
  }

  return cb(null, true);
};

const options = {
  storage,
  fileFilter: imageFileFilter,
};

const upload = multer(options);

exports.handleError = (err, req, res, next) => {
  if (err) {
    let errorMsg = '';
    switch (err.code) {
      case 'LIMIT_UNEXPECTED_FILE':
        if (!err.field) {
          errorMsg = `Unexpected file field: ${capitalize(err.field)}`;
        } else {
          errorMsg = `Number of files choosen for uploading are greater than ${constants[`maxCount${capitalize(err.field)}File`]}`;
        }
        break;
      case 'LIMIT_FILE_SIZE':
        errorMsg = 'Choosen file size is greater than allowed';
        break;
      case 'INVALID_FILE_TYPE':
        errorMsg = 'Choosen file is of invalid type';
        break;
      case 'ENOENT':
        errorMsg = 'Unable to store the file';
        break;
      default:
        errorMsg = `Something went wrong: ${err.code || 500}`;
    }

    return next(errors.badData(errorMsg));
  }

  if (req.files && req.files.length === 0) {
    return next(errors.badData('No files were uploaded'));
  }

  return next();
};

exports.upload = upload;
