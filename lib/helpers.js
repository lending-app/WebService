const errors = require('boom');

const verificationService = require('./verificationService');
const constants = require('../constants');
const User = require('../models/user');
const Role = require('../models/role');
const IdentityCheck = require('../models/identityCheck');

exports.isValidRole = async (req, res, next) => {
  try {
    const { type } = req.body;
    const role = await Role.findOne({ name: type });

    if (!role) {
      return next(errors.badData('Invalid Role type'));
    }

    req.role = role;
    return next();
  } catch (err) {
    return next(errors.boomify(err));
  }
};

exports.userExists = async (req, res, next) => {
  try {
    const { role } = req;
    const user = await User.findOne({ email: req.body.email }).populate('roles');

    if (user) {
      if (user.roles.some(item => item.name === role.name)) {
        return next(errors.badData('It seems that you already have an account. Please log-in'));
      }
    }

    req.user = user;
    return next();
  } catch (err) {
    return next(errors.boomify(err));
  }
};

exports.validateUserPendingChecks = async (req, res, next) => {
  try {
    const { userResource: user } = req;

    const idChecksAmount = await IdentityCheck.where({
      user,
      completed: false,
    }).countDocuments();

    if (idChecksAmount !== 0) {
      return next(errors.badData('You already have an ID Check in process.'));
    }

    return next();
  } catch (err) {
    return next(errors.boomify(err));
  }
};

exports.loadUser = async (req, res, next) => {
  const { userId } = req.params;
  try {
    const user = await User.findById(userId, constants.userFields.join(' '))
      .populate('roles');

    if (!user) {
      return next(errors.notFound(`User ${userId} not found`));
    }

    req.userResource = user;
    return next();
  } catch (err) {
    return next(errors.boomify(err));
  }
};

exports.loadUserIdCheck = async (req, res, next) => {
  const { idCheckId } = req.params;
  try {
    const idCheck = await IdentityCheck.findById(idCheckId);

    if (!idCheck) {
      return next(errors.notFound(`User's Identity Check ${idCheckId} not found`));
    }

    req.idCheck = idCheck;
    return next();
  } catch (err) {
    return next(errors.boomify(err));
  }
};

exports.validateIdCheckCompleted = (req, res, next) => {
  const { idCheck } = req;

  if (idCheck.completed) {
    return next(errors.badData('The ID Check has completed already'));
  }

  return next();
};


exports.validateIdCheckStatus = validStatus => (req, res, next) => {
  const { idCheck } = req;

  if (!validStatus.includes(idCheck.status)) {
    return next(errors.badData(`The ID Check is in status ${idCheck.status} and it must be in any of these statuses: ${validStatus.join(', ')}`));
  }

  return next();
};

exports.validateIdCheckImages = (req, res, next) => {
  const { idCheck } = req;

  if (!idCheck.images) {
    return next(errors.badData(`ID Check #${idCheck._id} doesn't have any images attached yet`));
  }

  return next();
};

exports.validateUploadedFiles = (req, res, next) => {
  const { files = null } = req;

  if (!files) {
    return next(errors.badData('It seems that no files were uploaded. Please try again'));
  }

  const { front = [], back = [], face = [] } = files;

  if (front.length === 0) {
    return next(errors.badData('No files were uploaded for front field. Please try again.'));
  } else if (front.length > constants.maxCountFrontFile) {
    return next(errors.badData(`You can upload a maximum of ${constants.maxCountFrontFile} files for front field`));
  }

  if (back.length === 0) {
    return next(errors.badData('No files were uploaded for back field. Please try again.'));
  } else if (back.length > constants.maxCountBackFile) {
    return next(errors.badData(`You can upload a maximum of ${constants.maxCountBackFile} files for back field`));
  }

  if (face.length === 0) {
    return next(errors.badData('No files were uploaded for face field. Please try again.'));
  } else if (face.length > constants.maxCountFaceFile) {
    return next(errors.badData(`You can upload a maximum of ${constants.maxCountFaceFile} files for face field`));
  }

  return next();
};

exports.updateIdCheckStatus = async (idCheck) => {
  try {
    const api = await verificationService;
    const { success, ...response } = await api.getVerification(idCheck);

    if (!success) {
      throw errors.badRequest(response.error.message || 'Something went wrong while fetching verification process data');
    }

    if (response.response.state === 'finished') {
      idCheck.set({ completed: true });

      if (!response.response.error || response.response.error === 'null') {
        idCheck.set({ status: 'completed' });
      } else {
        idCheck.set({ status: 'failed' });
      }

      idCheck.set({
        results: {
          results: response.response.results,
          faceComparison: response.response.face_comparison,
        },
      });
    }

    const updatedIdCheck = await idCheck.save().then(check => IdentityCheck.findById(check._id, '-user -__v').exec());

    return {
      success: true,
      idCheck: updatedIdCheck,
    };
  } catch (err) {
    let error = err;

    if (!errors.isBoom(err)) {
      error = errors.boomify(err);
    }

    return {
      success: false,
      error,
    };
  }
};
