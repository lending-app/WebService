const axios = require('axios');
const FormData = require('form-data');
const concat = require('concat-stream');
const path = require('path');
const fs = require('fs');
const config = require('../../config');

const { baseURL, accessToken } = config.verificationService;

const api = axios.create({
  baseURL,
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: `Bearer ${accessToken}`,
  },
});

const handleError = err => ({ success: false, error: err });

const configureClient = (entryPoint) => {
  const createVerification = async (idCheck) => {
    try {
      const response = await api.post(entryPoint, {
        webhook_url: `${config.apiUrl}/webhooks/verifications/${idCheck._id}`,
        reference_number: idCheck._id,
      });

      const {
        verification_url: url,
        verification_images_url: imagesUrl,
      } = response.data;
      const id = url.split('/')[2];

      return {
        success: true,
        verification: { id, url, imagesUrl },
        response: response.data,
      };
    } catch (err) {
      return handleError(err);
    }
  };

  const uploadImages = async (idCheck) => {
    try {
      if (!idCheck.images) {
        throw new Error(`ID Check #${idCheck._id} doesn't have any images to upload yet`);
      }

      const fields = Object.keys(idCheck.images);

      const uploadPhotos = fields.map(async (field) => {
        try {
          const uploadPhoto = new Promise((resolve) => {
            const fd = new FormData();

            const files = idCheck.images[field];

            fd.append('type', field);

            files.forEach((item) => {
              fd.append('file', fs.createReadStream(path.resolve(__basedir, item.path, item.filename)), item.filename);
            });

            fd.pipe(concat(data => resolve({ data, headers: fd.getHeaders() })));
          });

          const { data, headers } = await uploadPhoto;
          return api.post(`${entryPoint}/${idCheck.checkId}/images`, data, { headers });
        } catch (err) {
          throw new Error(err.message);
        }
      });

      const uploadedPhotos = await Promise.all(uploadPhotos);

      return {
        success: true,
        response: uploadedPhotos.map(upload => upload.data),
      };
    } catch (err) {
      return handleError(err);
    }
  };

  const startVerification = async (idCheck) => {
    try {
      const response = await api.put(`${entryPoint}/${idCheck.checkId}`);

      return {
        success: true,
        response: response.data,
      };
    } catch (err) {
      return handleError(err);
    }
  };

  const getVerification = async (idCheck) => {
    try {
      const response = await api.get(`${entryPoint}/${idCheck.checkId}`);

      return {
        success: true,
        response: response.data,
      };
    } catch (err) {
      return handleError(err);
    }
  };

  return {
    createVerification,
    uploadImages,
    startVerification,
    getVerification,
  };
};

exports.getEntryPoint = async () => {
  try {
    const response = await api.get('/');
    return { success: true, response: response.data };
  } catch (err) {
    return handleError(err);
  }
};

exports.configureClient = configureClient;
