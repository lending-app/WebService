const { configureClient, getEntryPoint } = require('./api');

module.exports = new Promise(async (resolve, reject) => {
  try {
    const response = await getEntryPoint();
    if (response.success) {
      const api = configureClient(response.response.verifications_url);
      return resolve(api);
    }

    return reject(response.error);
  } catch (err) {
    return reject(err);
  }
});
