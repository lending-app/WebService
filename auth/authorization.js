const errors = require('boom');

const isAdmin = (user) => {
  if (!user.roles || !user.roles.length) {
    return false;
  }

  return user.roles.some(role => role.name === 'admin');
};

const isUserOwner = (resource, user) => user.equals(resource);

const isUsersIdCheck = (user, idCheck) => user.equals(idCheck.user);

exports.verifyAdmin = (req, res, next) => (
  isAdmin(req.user)
    ? next()
    : next(errors.forbidden('Only administrators are allowed to perform this operation'))
);

exports.verifyNotAdmin = (req, res, next) => (
  !isAdmin(req.user)
    ? next()
    : next(new errors.Forbidden('Administrators are not allowed to perform this operation'))
);

exports.verifyUser = (req, res, next) => (
  isAdmin(req.user) || isUserOwner(req.userResource, req.user)
    ? next()
    : next(errors.forbidden('You are not authorized to perform this operation'))
);

exports.verifyIdCheck = (req, res, next) => (
  isAdmin(req.user) || isUsersIdCheck(req.userResource, req.idCheck)
    ? next()
    : next(errors.forbidden('You are not authorized to perform this operation'))
);
