const passport = require('passport');
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');
const jwt = require('jsonwebtoken');

const config = require('../config');
const User = require('../models/user');
const constants = require('../constants');

passport.use(User.createStrategy());

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

exports.getToken = user => jwt.sign(user, config.jwt.secretKey, {
  expiresIn: config.jwt.expiresIn,
});

const opts = {};
opts.passReqToCallback = true;
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.jwt.secretKey;

passport.use(new JwtStrategy(
  opts,
  async (req, jwtPayload, done) => {
    try {
      const user = await User.findOne(
        { _id: jwtPayload._id },
        constants.userFields.join(' '),
      )
        .populate('roles');

      if (user && user.roles.some(role => role.name === jwtPayload.role)) {
        req.isAdmin = jwtPayload.role === 'admin';
        return done(null, user);
      }

      return done(null, false);
    } catch (err) {
      return done(err, false);
    }
  },
));

exports.verifyUser = passport.authenticate('jwt', { session: false, failWithError: true });
