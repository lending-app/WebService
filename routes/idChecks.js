const express = require('express');
const errors = require('boom');
const cors = require('./cors');
const authorization = require('../auth/authorization');
const uploader = require('../lib/upload');
const helpers = require('../lib/helpers');
const verificationService = require('../lib/verificationService');
const constants = require('../constants');

const IdentityCheck = require('../models/identityCheck');

const router = express.Router();
router.use(express.json());

router.route('/')
  .all((req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    next();
  })
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus = 200;
  })
  /* GET users identity checks listing. */
  .get(async (req, res, next) => {
    try {
      const { userResource: user } = req;

      const idChecks = await IdentityCheck.find({ user }, '-user -__v');

      return res.json({ success: true, idChecks });
    } catch (err) {
      return next(errors.boomify(err));
    }
  })
  /* POST create new identity check for user. */
  .post(
    helpers.validateUserPendingChecks,
    async (req, res, next) => {
      try {
        const { userResource: user } = req;

        const idCheck = new IdentityCheck({
          user,
        });

        const api = await verificationService;
        const { success, ...response } = await api.createVerification(idCheck);

        if (!success) {
          return next(errors.badRequest(response.error.message || 'Something went wrong while creating verification process'));
        }

        const idChecksAmount = await IdentityCheck.where({
          checkId: response.verification.id,
        }).countDocuments();

        if (idChecksAmount !== 0) {
          return next(errors.badData('A verification process already exists with the same identifier. Please try again'));
        }

        idCheck.set({ checkId: response.verification.id });

        const newIdCheck = await idCheck.save().then(newCheck => IdentityCheck.findById(newCheck._id, '-user -__v').exec());

        return res.json({ success: true, idCheck: newIdCheck });
      } catch (err) {
        return next(errors.boomify(err));
      }
    },
  );

router.route('/:idCheckId')
  .all((req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    next();
  })
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus = 200;
  })
  /* GET user's specific identity check. */
  .get(
    helpers.loadUserIdCheck,
    authorization.verifyIdCheck,
    async (req, res, next) => {
      try {
        const { idCheck } = req;

        let requestedIdCheck = idCheck;

        if (!idCheck.completed) {
          const { success, ...result } = await helpers.updateIdCheckStatus(idCheck);

          if (!success) {
            return next(result.error);
          }

          requestedIdCheck = result.idCheck;
        }

        return res.json({ success: true, idCheck: requestedIdCheck });
      } catch (err) {
        return next(errors.boomify(err));
      }
    },
  )
  /* PUT start user's identity check verification process. */
  .put(
    helpers.loadUserIdCheck,
    authorization.verifyIdCheck,
    helpers.validateIdCheckCompleted,
    helpers.validateIdCheckStatus(['creating']),
    helpers.validateIdCheckImages,
    async (req, res, next) => {
      try {
        const { idCheck } = req;

        const api = await verificationService;
        const { success, ...response } = await api.startVerification(idCheck);

        if (!success) {
          return next(errors.badRequest(response.error.message || 'Something went wrong while starting verification process'));
        }

        idCheck.set({
          status: 'pending',
          startedAt: response.response.created_at,
        });

        const updatedIdCheck = await idCheck.save().then(check => IdentityCheck.findById(check._id, '-user -__v').exec());

        return res.json({ success: true, idCheck: updatedIdCheck });
      } catch (err) {
        return next(errors.boomify(err));
      }
    },
  );

router.route('/:idCheckId/images')
  .all((req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    next();
  })
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus = 200;
  })
  /* POST upload images for identity check process. */
  .post(
    helpers.loadUserIdCheck,
    authorization.verifyIdCheck,
    helpers.validateIdCheckCompleted,
    (req, res, next) =>
      uploader.upload.fields([
        { name: 'front', maxCount: constants.maxCountFrontFile },
        { name: 'back', maxCount: constants.maxCountBackFile },
        { name: 'face', maxCount: constants.maxCountFaceFile },
      ])(req, res, err => uploader.handleError(err, req, res, next)),
    helpers.validateUploadedFiles,
    async (req, res, next) => {
      try {
        const { idCheck } = req;
        const { front, back, face } = req.files;

        idCheck.set({
          images: {
            front: front.map(image => ({
              path: image.destination,
              filename: image.filename,
              mimetype: image.mimetype,
              size: image.size,
            })),
            back: back.map(image => ({
              path: image.destination,
              filename: image.filename,
              mimetype: image.mimetype,
              size: image.size,
            })),
            face: face.map(image => ({
              path: image.destination,
              filename: image.filename,
              mimetype: image.mimetype,
              size: image.size,
            })),
          },
        });

        const api = await verificationService;
        const { success, ...response } = await api.uploadImages(idCheck);

        if (!success) {
          let errorMsg = '';
          if (!response.error) {
            errorMsg = 'Something went wrong while uploading images to verification process';
          } else if (response.error instanceof Array) {
            errorMsg = response.error.map(err => err.message || '').join('\n');
          } else {
            errorMsg = response.error.message;
          }

          return next(errors.badRequest(errorMsg));
        }

        const updatedIdCheck = await idCheck.save().then(newCheck => IdentityCheck.findById(newCheck._id, '-user -__v').exec());

        return res.json({ success: true, idCheck: updatedIdCheck });
      } catch (err) {
        return next(errors.boomify(err));
      }
    },
    (err, req, res, next) => next(err),
  );

module.exports = router;
