const express = require('express');
const cors = require('./cors');
const helpers = require('../lib/helpers');

const IdentityCheck = require('../models/identityCheck');

const router = express.Router();

router.all('/*', (req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  next();
});

router.options('/verifications/:idCheckId', cors.cors, (req, res) => {
  res.sendStatus(200);
});

/* GET home page. */
router.post(
  '/verifications/:idCheckId',
  cors.cors,
  helpers.loadUserIdCheck,
  helpers.validateIdCheckCompleted,
  async (req, res, next) => {
    try {
      const { event, data } = req.body;

      switch (event) {
        case 'verification.finished': {
          const idCheck = await IdentityCheck.find({ checkId: data.id });
          const { success, ...result } = await helpers.updateIdCheckStatus(idCheck);

          if (!success) {
            console.error(`ERROR PROCESSING WEBHOOK NOTIFICATION - UPDATING ID CHECK #${idCheck._id}`);
            console.error(result.error);
            break;
          }

          console.log(JSON.stringify(result, null, 2));
          break;
        }
        default:
          break;
      }

      return res.sendStatus(200);
    } catch (error) {
      console.error('ERROR PROCESSING WEBHOOK NOTIFICATION');
      console.error(error);
      return res.sendStatus(200);
    }
  },
);

module.exports = router;
