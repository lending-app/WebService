const express = require('express');
const errors = require('boom');
const cors = require('./cors');
const authenticate = require('../auth/authenticate');
const authorization = require('../auth/authorization');
const helpers = require('../lib/helpers');
const idChecksRouter = require('./idChecks');

const User = require('../models/user');

const router = express.Router();
router.use(express.json());

router.route('/')
  .all((req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    next();
  })
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus = 200;
  })
  /* GET users listing. */
  .get(
    cors.corsWithOptions,
    authenticate.verifyUser,
    authorization.verifyAdmin,
    async (req, res, next) => {
      try {
        const users = await User.find({ email: { $ne: req.user.email } });

        return res.json({ success: true, users });
      } catch (err) {
        return next(errors.boomify(err));
      }
    },
  );

router.route('/:userId')
  .all((req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    next();
  })
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus = 200;
  })
  /* GET users info. */
  .get(
    cors.corsWithOptions,
    authenticate.verifyUser,
    helpers.loadUser,
    authorization.verifyUser,
    (req, res, next) => {
      const { userResource: user } = req;

      return res.json({ success: true, user });
    },
  );

router.use(
  '/:userId/identitycheck',
  cors.corsWithOptions,
  authenticate.verifyUser,
  helpers.loadUser,
  authorization.verifyUser,
  idChecksRouter,
);

module.exports = router;
