const express = require('express');
const passport = require('passport');
const errors = require('boom');
const cors = require('./cors');
const authenticate = require('../auth/authenticate');
const helpers = require('../lib/helpers');

const User = require('../models/user');

const router = express.Router();
router.use(express.json());

router.all('*', (req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  next();
});

router.options('/*', cors.corsWithOptions);

router.post(
  '/email',
  cors.corsWithOptions,
  helpers.isValidRole,
  helpers.userExists,
  (req, res, next) => {
    let result = true;

    if (req.user) {
      result = false;
    }

    return res.json({ success: true, result });
  },
);

router.post(
  '/register',
  cors.corsWithOptions,
  helpers.isValidRole,
  helpers.userExists,
  async (req, res, next) => {
    const { role, user } = req;

    if (user) {
      try {
        await user.update({ $addToSet: { roles: role } });
      } catch (err) {
        return next(errors.boomify(err));
      }
    } else {
      try {
        const roles = role !== null ? [role] : [];

        const newUser = new User({
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
          birthdate: req.body.birthdate,
          phoneNumber: req.body.phoneNumber,
          roles,
        });

        await User.register(newUser, req.body.password);
      } catch (err) {
        return next(errors.badData(err.message));
      }
    }

    return passport.authenticate('local', { session: false, failWithError: true })(req, res, async () => {
      if (!req.user) {
        return next(errors.boomify(new Error(), { message: 'There was an error registering the user' }));
      }

      const token = authenticate.getToken({ _id: req.user._id, role: role.name });

      return res.json({
        success: true,
        user: req.user,
        token,
        message: 'Registration Successful!',
      });
    });
  },
);

router.post(
  '/login',
  cors.corsWithOptions,
  helpers.isValidRole,
  passport.authenticate('local', { session: false, failWithError: true }),
  (req, res, next) => {
    const { role, user } = req;
    const token = authenticate.getToken({ _id: user._id, role: role.name });
    return res.json({
      success: true,
      user,
      token,
      message: 'You are successfully logged in',
    });
  },
  (err, req, res, next) => next(errors.unauthorized('Authentication error')),
);

router.post(
  '/logout',
  cors.corsWithOptions,
  authenticate.verifyUser,
  (req, res, next) => {
    if (req.user) {
      req.logout();
      return res.json({ success: true, user: {}, status: 'Successfully logged out' });
    }

    return next(errors.unauthorized('You are not logged in!'));
  },
);

module.exports = router;
