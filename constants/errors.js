module.exports = {
  MissingPasswordError: 'No password was given',
  AttemptTooSoonError: 'Account is currently locked. Try again later',
  TooManyAttemptsError: 'Account locked due to too many failed login attempts',
  NoSaltValueStoredError: 'Authentication not possible. No salt value stored',
  IncorrectPasswordError: 'Password or username is incorrect',
  IncorrectUsernameError: 'Password or username is incorrect',
  MissingUsernameError: 'No username was given',
  UserExistsError: 'A user with the given email is already registered',
};
