const mongoose = require('mongoose');
const constants = require('../constants');

const { Schema } = mongoose;

const File = new Schema({
  path: {
    type: String,
    default: null,
  },
  filename: {
    type: String,
    default: null,
  },
  mimetype: {
    type: String,
    default: null,
  },
  size: {
    type: Number,
    default: null,
  },
  _id: false,
});

const IdentityCheck = new Schema({
  checkId: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
    unique: true,
  },
  completed: {
    type: Boolean,
    required: true,
    default: false,
  },
  status: {
    type: String,
    required: true,
    default: 'creating',
    enum: ['creating', 'pending', 'completed', 'failed'],
  },
  startedAt: {
    type: Date,
    default: null,
  },
  images: {
    type: {
      front: {
        type: [File],
        default: undefined,
        validate: [val => val.length <= constants.maxCountFrontFile, `{PATH} exceeds the limit of ${constants.maxCountFrontFile}`],
      },
      back: {
        type: [File],
        default: undefined,
        validate: [val => val.length <= constants.maxCountBackFile, `{PATH} exceeds the limit of ${constants.maxCountBackFile}`],
      },
      face: {
        type: [File],
        default: undefined,
        validate: [val => val.length <= constants.maxCountFaceFile, `{PATH} exceeds the limit of ${constants.maxCountFaceFile}`],
      },
    },
    default: null,
  },
  user: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  results: {
    type: Schema.Types.Mixed,
    default: null,
  },
}, {
  timestamps: true,
});

module.exports = mongoose.model('IdentityCheck', IdentityCheck);
