const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const { errors } = require('passport-local-mongoose');
const validate = require('mongoose-validator');
const validator = require('validator');
const sanitizerPlugin = require('mongoose-sanitizer-plugin');
const moment = require('moment');
const { isValidNumber } = require('libphonenumber-js');
const { namePattern, passwordPattern } = require('../constants');
const errorMessages = require('../constants/errors');

const { Schema } = mongoose;

const firstNameValidator = [
  validate({
    validator: 'matches',
    arguments: [namePattern, 'g'],
    message: 'First name must not contain numbers',
    httpStatus: 422,
  }),
];

const lastNameValidator = [
  validate({
    validator: 'matches',
    arguments: [namePattern, 'g'],
    message: 'Last name must not contain numbers',
    httpStatus: 422,
  }),
];

const emailValidator = [
  validate({
    validator: 'isEmail',
    message: 'Please enter a valid E-mail',
    httpStatus: 422,
  }),
];

const passwordValidator = (password, cb) => {
  if (validator.isEmpty(password)) {
    return cb({
      code: 422,
      message: 'Password is required to register',
    });
  }

  if (!validator.isLength(password, { min: 8 })) {
    return cb({
      code: 422,
      message: 'Password must be at least 8 characters long',
    });
  }

  if (!validator.matches(password, passwordPattern, 'g')) {
    return cb({
      code: 422,
      message: 'Enter a valid password: Must have at least an upper case letter and a number',
    });
  }

  return cb(null);
};

const birthdateValidator = [
  // validate({
  //   validator: 'isBefore',
  //   arguments: moment().toString(),
  //   message: 'Please enter a valid Birthdate',
  //   httpStatus: 422,
  // }),
  validate({
    validator(val) {
      const birthdate = moment(val);
      return moment().diff(birthdate, 'years') >= 18;
    },
    message: 'You must be at least 18 years old',
    httpStatus: 422,
  }),
];

const phoneNumberValidator = [
  validate({
    validator(val) {
      return isValidNumber(val, 'CO');
    },
    message: 'Please enter a valid Phone Number',
    httpStatus: 422,
  }),
];

const User = new Schema({
  firstName: {
    type: String,
    lowercase: true,
    trim: true,
    required: true,
    validate: firstNameValidator,
  },
  lastName: {
    type: String,
    lowercase: true,
    trim: true,
    required: true,
    validate: lastNameValidator,
  },
  email: {
    type: String,
    trim: true,
    required: true,
    validate: emailValidator,
  },
  phoneNumber: {
    type: String,
    required: true,
    validate: phoneNumberValidator,
  },
  birthdate: {
    type: Date,
    required: true,
    validate: birthdateValidator,
  },
  passwordReset: {
    type: String,
    select: false,
  },
  roles: [{
    type: Schema.Types.ObjectId,
    ref: 'Role',
  }],
}, {
  timestamps: true,
});

User.plugin(passportLocalMongoose, {
  usernameField: 'email',
  usernameLowerCase: true,
  errorMessages,
  populateFields: 'roles',
  passwordValidator,
  passReqToCallback: true,
});

/* eslint-disable func-names */
User.statics.authenticate = function () {
  return (req, username, password, cb) => {
    const promise = Promise.resolve()
      .then(() => this.findByUsername(username, true))
      .then((user) => {
        const { role } = req;

        if (user && user.roles.some(item => item.name === role.name)) {
          return user.authenticate(password);
        }

        return {
          user: false,
          error: new errors.IncorrectUsernameError(errorMessages.IncorrectUsernameError),
        };
      });

    if (!cb) {
      return promise;
    }

    return promise
      .then(({ user, error }) => cb(null, user, error))
      .catch(err => cb(err));
  };
};
/* eslint-disable ds */

User.plugin(sanitizerPlugin, {
  mode: 'sanitize',
  exclude: ['roles'],
});

module.exports = mongoose.model('User', User);
